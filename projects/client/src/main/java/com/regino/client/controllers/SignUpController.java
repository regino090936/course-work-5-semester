package com.regino.client.controllers;

import com.regino.client.connection.Request;
import com.regino.client.connection.Response;
import com.regino.client.connection.model.UserSerDes;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.IOException;

public class SignUpController {

    public CheckBox isAdmin;

    @FXML
    private TextField company;

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField name;

    @FXML
    private TextField surname;

    @FXML
    private TextField email;

    @FXML
    private TextField country;

    @FXML
    private TextField city;

    @FXML
    private TextField position;

    @FXML
    private Text output;

    @FXML
    void signUp(ActionEvent event) throws IOException {
        Request request = new Request("signup");
        UserSerDes user = new UserSerDes(
                company.getText(),
                country.getText(),
                city.getText(),
                name.getText(),
                surname.getText(),
                email.getText(),
                position.getText(),
                loginField.getText(),
                isAdmin.isSelected() ? "true" : "false"
        );
        System.out.println(user);
        request.setBody(user);
        request.addAttribute("pass", passwordField.getText());
        Response response = request.send();

        StringBuilder str = new StringBuilder();
        response.getAttrsMap().forEach((key, value) -> str.append(key).append(" = ").append(value));

        output.setText(str.toString());
    }

    @FXML
    void getBack(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(output.getScene(), getClass(), "authScene");
    }


}
