package com.regino.client.connection.model;

public class OrgTypeSerDes {
    private String name;

    public OrgTypeSerDes(String name) {
        this.name = name;
    }

    public OrgTypeSerDes() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
