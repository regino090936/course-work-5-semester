package com.regino.client.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.regino.client.connection.Request;
import com.regino.client.connection.Response;
import com.regino.client.connection.model.IncomeSerDes;
import com.regino.client.connection.model.UserSerDes;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

public class UserSceneController {

    private static UserSerDes profile;
    private boolean isExecutable;

    public static UserSerDes getProfile() {
        return profile;
    }

    public static void setProfile(UserSerDes profile) {
        UserSceneController.profile = profile;
    }

    @FXML
    private Text header;

    @FXML
    private TextField incomeField;

    @FXML
    private TextField yearField;

    @FXML
    private Button btn1;

    @FXML
    private Button btn3;

    @FXML
    private Button btn2;

    @FXML
    private Text errorText;

    @FXML
    private TableView<IncomeToTable> table;

    @FXML
    private TableColumn<IncomeToTable, Integer> year;

    @FXML
    private TableColumn<IncomeToTable, String> winter;

    @FXML
    private TableColumn<IncomeToTable, String> spring;

    @FXML
    private TableColumn<IncomeToTable, String> summer;

    @FXML
    private TableColumn<IncomeToTable, String> autumn;

    private List<IncomeSerDes> incomes;
    private final Type incomeType = new TypeToken<List<IncomeSerDes>>() {}.getType();

    @FXML
    void seeProfile(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(btn1.getScene(), getClass(), "userInfoScene");
    }

    @FXML
    void companyInfo(ActionEvent event) throws IOException {
        CompanyInfoController.setCompanyName(profile.getCompanyName());
        CommonFunctions.openNewWindow(btn1.getScene(), getClass(), "companyInfoScene");
    }

    private String report;

    @FXML
    void makeForecast(ActionEvent event) throws IOException {
        Request request = new Request("forecast");
        request.addAttribute("companyName", profile.getCompanyName());
        request.addAttribute("yearBegin", yearField.getText());
        Response response = request.send();
        report = response.getAttr("report");
        makeExecutable();
        System.out.println(Integer.parseInt(response.getAttr("year")));
        System.out.println(Double.parseDouble(response.getAttr("1")));
        System.out.println(Double.parseDouble(response.getAttr("2")));
        System.out.println(Double.parseDouble(response.getAttr("3")));
        System.out.println(Double.parseDouble(response.getAttr("4")));

        LineChartController.setForecastIncome(new IncomeSerDes(
                Integer.parseInt(response.getAttr("year")),
                Double.parseDouble(response.getAttr("1")),
                Double.parseDouble(response.getAttr("2")),
                Double.parseDouble(response.getAttr("3")),
                Double.parseDouble(response.getAttr("4"))
        ));
    }

    @FXML
    void textReport(ActionEvent event) throws IOException {
        BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream("report.txt"));
        outputStream.write(report.getBytes());
        outputStream.close();
    }

    @FXML
    void diagramByQuarter(ActionEvent event) throws IOException {
        LineChartController.setIncomes(incomes);
        CommonFunctions.openNewWindow(btn1.getScene(), getClass(), "pieChartScene");
    }

    @FXML
    void diagramByYears(ActionEvent event) throws IOException {
        LineChartController.setIncomes(incomes);
        CommonFunctions.openNewWindow(btn1.getScene(), getClass(), "lineChartScene");
    }

    @FXML
    void replace(ActionEvent event) throws IOException {
       editIncome(true);
    }

    @FXML
    void addToCurrent(ActionEvent event) throws IOException {
        editIncome(false);
    }

    private void editIncome(boolean isReplace) throws IOException {
        Request request = new Request("editIncome");
        request.addAttribute("companyName", profile.getCompanyName());
        if (isReplace)
            request.addAttribute("isReplace", "true");
        else
            request.addAttribute("isReplace", "false");

        String[] elements;
        try {
            elements = incomeField.getText().replaceAll(" ", "").split(",");
            if (elements.length != 5)
                throw new Exception();

            for (String s : elements) {
                int i = Integer.parseInt(s);
                if (i < 0)
                    throw new Exception();
            }
        } catch (Exception e) {
            errorText.setText("неправильный формат");
            return;
        }
        request.addAttribute("year", elements[0]);
        request.addAttribute("1", elements[1]);
        request.addAttribute("2", elements[2]);
        request.addAttribute("3", elements[3]);
        request.addAttribute("4", elements[4]);
        request.send();
        update();
    }


    @FXML
    void getBack(ActionEvent event) throws IOException {
        setProfile(null);
        CommonFunctions.switchToNewWindow(btn1.getScene(), getClass(), "authScene");
    }

    private void makeExecutable() {
        isExecutable = true;
        btn1.setOpacity(1);
        btn2.setOpacity(1);
        btn3.setOpacity(1);
    }

    private void makeNotExecutable() {
        isExecutable = false;
        btn1.setOpacity(0.2);
        btn2.setOpacity(0.2);
        btn3.setOpacity(0.2);
    }

    public static class IncomeToTable {
        private final int year;
        private final String winter;
        private final String spring;
        private final String summer;
        private final String autumn;

        public IncomeToTable(IncomeSerDes i) {
            year = i.getYear();
            winter = String.format("%.2f", i.getWinter());
            spring = String.format("%.2f", i.getSpring());
            summer = String.format("%.2f", i.getSummer());
            autumn = String.format("%.2f", i.getAutumn());
        }

        public int getYear() {
            return year;
        }

        public String getWinter() {
            return winter;
        }

        public String getSpring() {
            return spring;
        }

        public String getSummer() {
            return summer;
        }

        public String getAutumn() {
            return autumn;
        }
    }

    @FXML
    void update() throws IOException {
        if (profile == null) {
            header.setText("Something went wrong");
            return;
        }
        makeNotExecutable();
        header.setText(profile.getCompanyName());
        Request request = new Request("getIncomes");
        ObservableList<IncomeToTable> obList;
        request.addAttribute("name", profile.getCompanyName());
        Response response = request.send();

        incomes = new Gson().fromJson(response.getResponseBody(), incomeType);
        obList = FXCollections.observableArrayList(incomes.stream().map(IncomeToTable::new).collect(Collectors.toList()));

        year.setCellValueFactory(new PropertyValueFactory<IncomeToTable, Integer>("year"));
        winter.setCellValueFactory(new PropertyValueFactory<IncomeToTable, String>("winter"));
        spring.setCellValueFactory(new PropertyValueFactory<IncomeToTable, String>("spring"));
        summer.setCellValueFactory(new PropertyValueFactory<IncomeToTable, String>("summer"));
        autumn.setCellValueFactory(new PropertyValueFactory<IncomeToTable, String>("autumn"));

        table.setItems(obList);
    }

    @FXML
    void initialize() throws IOException {
        update();
    }
}
