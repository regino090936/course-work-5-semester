package com.regino.client.connection.model;

public class CompanySerDes {
    private String name;
    private int employeeAmount;
    private double averageIncome;

    public CompanySerDes(String name, int employeeAmount, double averageIncome) {
        this.name = name;
        this.employeeAmount = employeeAmount;
        this.averageIncome = averageIncome;
    }

    public CompanySerDes() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEmployeeAmount() {
        return employeeAmount;
    }

    public void setEmployeeAmount(int employeeAmount) {
        this.employeeAmount = employeeAmount;
    }

    public double getAverageIncome() {
        return averageIncome;
    }

    public void setAverageIncome(double averageIncome) {
        this.averageIncome = averageIncome;
    }

    @Override
    public String toString() {
        return String.format(
                "Название: %s, Количество сотрудников: %d, Средняя прибыль: %.2f",
                getName(),
                getEmployeeAmount(),
                getAverageIncome()
        );
    }
}
