package com.regino.client.connection.model;

public class IncomeSerDes {
    private int year;
    private double winter;
    private double spring;
    private double summer;
    private double autumn;

    public IncomeSerDes(int year, double winter, double spring, double summer, double autumn) {
        this.year = year;
        this.winter = winter;
        this.spring = spring;
        this.summer = summer;
        this.autumn = autumn;
    }

    public IncomeSerDes() {
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getWinter() {
        return winter;
    }

    public void setWinter(double winter) {
        this.winter = winter;
    }

    public double getSpring() {
        return spring;
    }

    public void setSpring(double spring) {
        this.spring = spring;
    }

    public double getSummer() {
        return summer;
    }

    public void setSummer(double summer) {
        this.summer = summer;
    }

    public double getAutumn() {
        return autumn;
    }

    public void setAutumn(double autumn) {
        this.autumn = autumn;
    }

    public double getSum() {
        return winter + spring + summer + autumn;
    }

    @Override
    public String toString() {
        return String.format(
                "Year: %d, winter - %.2f, spring - %.2f, summer - %.2f, autumn - %.2f",
                this.year,
                this.winter,
                this.spring,
                this.summer,
                this.autumn
        );
    }
}
