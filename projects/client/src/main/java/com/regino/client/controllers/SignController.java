package com.regino.client.controllers;

import com.google.gson.Gson;
import com.regino.client.connection.Connection;
import com.regino.client.connection.Request;
import com.regino.client.connection.Response;
import com.regino.client.connection.model.UserSerDes;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class SignController {
    public TextField loginField;
    public PasswordField passwordField;

    @FXML
    private Text errorText;

    @FXML
    void exit(ActionEvent event) throws IOException {
        Connection.closeConnection();
        errorText.getScene().getWindow().hide();
    }

    public void authorize() throws IOException {
        String login = loginField.getCharacters().toString();
        String password = passwordField.getCharacters().toString();

        Request request = new Request("auth");
        request.addAttribute("login", login);
        request.addAttribute("pass", password);

        Response response = request.send();

        System.out.println(response.getAttr("isAuthorised"));
        if (response.isAttrMatch("isAuthorised", "true")) {
            String sceneAdress;

            if (response.isAttrMatch("isAdmin", "true")) {
                sceneAdress = "/views/adminScene.fxml";
            } else {
                sceneAdress = "/views/userScene.fxml";
                UserSceneController.setProfile(new Gson().fromJson(response.getResponseBody(), UserSerDes.class));
            }
            System.out.println("authorized");
            loginField.getScene().getWindow().hide();
            Stage mainStage = new Stage();
            mainStage.setScene(new Scene(new FXMLLoader(getClass().getResource(sceneAdress)).load()));
            mainStage.show();
            ListView<String> listView = new ListView<String>();
        } else {
            errorText.setText("Совпадений не найдено");
        }
    }

    public void signUp(ActionEvent event) throws IOException {
        loginField.getScene().getWindow().hide();
        Stage mainStage = new Stage();
        mainStage.setScene(new Scene(new FXMLLoader(getClass().getResource("/views/signUpScene.fxml")).load()));
        mainStage.show();
    }
}
