package com.regino.client.connection.model;

public class UserSerDes {

    private String companyName;
    private String country;
    private String city;
    private String name;
    private String surname;
    private String email;
    private String position;
    private String login;
    private String status;

    public UserSerDes(String companyName, String country, String city, String name, String surname, String email, String position, String login, String status) {
        this.companyName = companyName;
        this.country = country;
        this.city = city;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.position = position;
        this.login = login;
        this.status = status;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPosition() {
        return position;
    }

    public String getLogin() {
        return login;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return String.format(
                "Name: %s, Surname: %s, Email: %s, Login: %s, Company: %s, Position: %s, Country: %s, City: %s, STATUS: %s",
                getName(),
                getSurname(),
                getEmail(),
                getLogin(),
                getCompanyName(),
                getPosition(),
                getCountry(),
                getCity(),
                getStatus()
        );
    }
}
