package com.regino.client.connection;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.Map;

public class Request {
    private final String requestType;
    private final Map<String, String> attrMap;
    private String body;

    public Request(String requestType, Map<String, String> attrMap) {
        this.requestType = requestType;
        this.attrMap = attrMap;
    }

    public Request(String requestType) {
        this.requestType = requestType;
        this.attrMap = new HashMap<>();
    }

    public void setBody(Object obj) {
        this.body = new Gson().toJson(obj);
    }

    public String getAttribute(String name) {
        return attrMap.get(name);
    }

    public void addAttribute(String name, String value) {
        attrMap.put(name, value);
    }

    public String getRequestType() {
        return requestType;
    }

    public Response send() throws IOException {
        return Connection.sendRequest(this);
    }
}
