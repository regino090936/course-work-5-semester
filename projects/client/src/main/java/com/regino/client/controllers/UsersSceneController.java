package com.regino.client.controllers;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.regino.client.connection.Request;
import com.regino.client.connection.Response;
import com.regino.client.connection.model.UserSerDes;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

public class UsersSceneController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField pattern;

    @FXML
    private Text errorText;

    @FXML
    private CheckBox adminCheckbox;

    @FXML
    private CheckBox userChechbox;

    @FXML
    private TableView<UserSerDes> table;

    @FXML
    private TableColumn<UserSerDes, String> name;

    @FXML
    private TableColumn<UserSerDes, String> surname;

    @FXML
    private TableColumn<UserSerDes, String> email;

    @FXML
    private TableColumn<UserSerDes, String> login;

    @FXML
    private TableColumn<UserSerDes, String> company;

    @FXML
    private TableColumn<UserSerDes, String> position;

    @FXML
    private TableColumn<UserSerDes, String> country;

    @FXML
    private TableColumn<UserSerDes, String> city;

    @FXML
    private TableColumn<UserSerDes, String> status;


    @FXML
    void addUser(ActionEvent event) throws IOException {
        CommonFunctions.openNewWindow(userChechbox.getScene(), getClass(), "signUpScene");
    }

    @FXML
    void adminCheckboxAction(ActionEvent event) throws IOException {
        update();
    }

    @FXML
    void userCheckboxAction(ActionEvent event) throws IOException {
        update();
    }

    @FXML
    void deleteAll(ActionEvent event) {

    }

    @FXML
    void deleteSelected(ActionEvent event) throws IOException {
        int index = table.getFocusModel().getFocusedIndex();
        Request request = new Request("userDelete");
        request.addAttribute("login", users.get(index).getLogin());
        request.send();
        update();
    }

    @FXML
    void findLike(ActionEvent event) throws IOException {
        String[] params = pattern.getText().replaceAll(" ", "").split(":");
        if (params.length < 2) {
            errorText.setText("Неправильный формат");
            return;
        }
        Request request = new Request("filtrUsers");
        request.addAttribute("filtrField", params[0].toLowerCase());
        request.addAttribute("value", params[1].toLowerCase());
        Response response = request.send();
        users = new Gson().fromJson(response.getResponseBody(), listType);
        if (users == null) {
            errorText.setText("Совпадений не найдено");
            return;
        }
        printList();
    }

    private enum SortType {
        EMAIL_SORT,
        LOGIN_SORT,
        NAME_SORT,
        SURNAME_SORT
    }
    private SortType sortType = SortType.EMAIL_SORT;
    private boolean isDirect = true;

    @FXML
    void sortEmail(ActionEvent event) {
        if (sortType != SortType.EMAIL_SORT) {
            sortType = SortType.EMAIL_SORT;
            isDirect = true;
        }

        if (isDirect) {
            users.sort((n1, n2) -> n1.getEmail().compareTo(n2.getEmail()));
        } else {
            users.sort((n1, n2) -> n2.getEmail().compareTo(n1.getEmail()));
        }
        isDirect = !isDirect;
        printList();
    }

    @FXML
    void sortLogin(ActionEvent event) {
        if (sortType != SortType.LOGIN_SORT) {
            sortType = SortType.LOGIN_SORT;
            isDirect = true;
        }

        if (isDirect) {
            users.sort((n1, n2) -> n1.getLogin().compareTo(n2.getLogin()));
        } else {
            users.sort((n1, n2) -> n2.getLogin().compareTo(n1.getLogin()));
        }
        isDirect = !isDirect;
        printList();
    }

    @FXML
    void sortName(ActionEvent event) {
        if (sortType != SortType.NAME_SORT) {
            sortType = SortType.NAME_SORT;
            isDirect = true;
        }

        if (isDirect) {
            users.sort((n1, n2) -> n1.getName().compareTo(n2.getName()));
        } else {
            users.sort((n1, n2) -> n2.getName().compareTo(n1.getName()));
        }
        isDirect = !isDirect;
        printList();
    }

    @FXML
    void sortSurname(ActionEvent event) {
        if (sortType != SortType.SURNAME_SORT) {
            sortType = SortType.SURNAME_SORT;
            isDirect = true;
        }

        if (isDirect) {
            users.sort((n1, n2) -> n1.getSurname().compareTo(n2.getSurname()));
        } else {
            users.sort((n1, n2) -> n2.getSurname().compareTo(n1.getSurname()));
        }
        isDirect = !isDirect;
        printList();
    }

    private List<UserSerDes> users;
    private final Type listType = new TypeToken<List<UserSerDes>>() {}.getType();

    @FXML
    void getBack(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(userChechbox.getScene(), getClass(), "AdminScene");
    }

    @FXML
    void update() throws IOException {
        Request request = new Request("users");
        Response response = request.send();
        users = new Gson().fromJson(response.getResponseBody(), listType);
        printList();
    }

    private void printList() {
        ObservableList<UserSerDes> locs = FXCollections.observableArrayList();
        locs.addAll(users.stream()
                .filter(n -> {
                    System.out.println(n.getStatus());
                    if (userChechbox.isSelected() && n.getStatus().equals("User")) {
                        return true;
                    } else if (adminCheckbox.isSelected() && n.getStatus().equals("Admin")) {
                        return true;
                    }
                    return false;
                })
                .collect(Collectors.toList())
        );

        name.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("name"));
        surname.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("surname"));
        email.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("email"));
        login.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("login"));
        company.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("companyName"));
        position.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("position"));
        country.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("country"));
        city.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("city"));
        status.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("status"));

        table.setItems(locs);
    }

    @FXML
    void initialize() throws IOException {
        userChechbox.setSelected(true);
        adminCheckbox.setSelected(true);
        update();
    }
}
