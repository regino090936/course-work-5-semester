package com.regino.client.connection;

import com.google.gson.Gson;

import java.io.*;
import java.net.ConnectException;
import java.net.Socket;

public class Connection {
    private static Socket socket;
    private static BufferedReader in;
    private static PrintWriter out;
    private static final Gson gson = new Gson();

    private Connection() {}

    public static void setConnection(String host, int port) throws IOException {
        socket = new Socket(host, port);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new BufferedOutputStream(socket.getOutputStream()));
    }

    public static Response sendRequest(Request request) throws IOException {
        if (socket == null) {
            throw new ConnectException();
        }
        String jsonMsg = gson.toJson(request);
        out.println(jsonMsg);
        out.flush();
        String msg = in.readLine();
        System.out.println("recieved msg - " + msg);
        return gson.fromJson(msg, Response.class);

    }

    public static void closeConnection() throws IOException {
        socket.close();
    }
}
