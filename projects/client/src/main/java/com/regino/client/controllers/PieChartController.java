package com.regino.client.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;

public class PieChartController {

    @FXML
    private PieChart chart;

    @FXML
    void initialize() {
        ObservableList<PieChart.Data> pieChartData
                = FXCollections.observableArrayList(
                    new PieChart.Data("winter", LineChartController.getForecastIncome().getWinter()),
                    new PieChart.Data("spring", LineChartController.getForecastIncome().getSpring()),
                    new PieChart.Data("summer", LineChartController.getForecastIncome().getSummer()),
                    new PieChart.Data("autumn", LineChartController.getForecastIncome().getAutumn())
        );
        chart.setData(pieChartData);
    }
}
