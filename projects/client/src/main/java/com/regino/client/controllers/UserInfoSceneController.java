package com.regino.client.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;

public class UserInfoSceneController {

    @FXML
    private Text header;

    @FXML
    private Text name;

    @FXML
    private Text surname;

    @FXML
    private Text login;

    @FXML
    private Text position;

    @FXML
    private Text email;

    @FXML
    private Text company;

    @FXML
    private Text country;

    @FXML
    private Text city;

    @FXML
    void getBack() throws IOException {
        CommonFunctions.switchToNewWindow(city.getScene(), getClass(), "userScene");
    }

    @FXML
    void initialize() throws IOException {
        if (UserSceneController.getProfile() == null) {
            header.setText("Profile not found");
            getBack();
            return;
        }

        name.setText(UserSceneController.getProfile().getName());
        surname.setText(UserSceneController.getProfile().getSurname());
        email.setText(UserSceneController.getProfile().getEmail());
        position.setText(UserSceneController.getProfile().getPosition());
        login.setText(UserSceneController.getProfile().getLogin());
        company.setText(UserSceneController.getProfile().getCompanyName());
        country.setText(UserSceneController.getProfile().getCountry());
        city.setText(UserSceneController.getProfile().getCity());
    }
}
