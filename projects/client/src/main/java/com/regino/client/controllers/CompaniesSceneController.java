package com.regino.client.controllers;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.regino.client.connection.Request;
import com.regino.client.connection.Response;
import com.regino.client.connection.model.CompanySerDes;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

public class CompaniesSceneController {

    private List<CompanySerDes> companies;
    private final Type listType = new TypeToken<List<CompanySerDes>>() {}.getType();

    @FXML
    private TableView<CompanySerDes> table;

    @FXML
    private TableColumn<CompanySerDes, String> nameColumn;

    @FXML
    private TableColumn<CompanySerDes, Integer> amountColumn;

    @FXML
    private TableColumn<CompanySerDes, Double> incomeColumn;

    private ObservableList<CompanySerDes> companiesList;

    @FXML
    private TextField pattern;

    @FXML
    private Text errorText;

    @FXML
    void addCompany(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(errorText.getScene(), getClass(), "addingCompanyScene");
    }

    @FXML
    void companyInfo(ActionEvent event) throws IOException {
        int index = table.getFocusModel().getFocusedIndex();
        CompanyInfoController.setCompanyName(companies.get(index).getName());
        CommonFunctions.openNewWindow(errorText.getScene(), getClass(), "companyInfoScene");
    }

    @FXML
    void findLike() throws IOException {
        Request request = new Request("findCompany");
        request.addAttribute("value", pattern.getText());
        Response response = request.send();
        companies = new Gson().fromJson(response.getResponseBody(), listType);
        printList();
    }

    private enum SortType {
        AMOUNT_SORT,
        INCOME_SORT,
        NAME_SORT
    }
    private SortType sortType = SortType.NAME_SORT;
    private boolean isDirect = true;

    @FXML
    void sortByEmployeeAmount(ActionEvent event) {
        if (sortType != SortType.AMOUNT_SORT) {
            sortType = SortType.AMOUNT_SORT;
            isDirect = true;
        }

        companies.sort((n1, n2) -> (n2.getEmployeeAmount() - n1.getEmployeeAmount()) * (isDirect ? 1 : -1));
        isDirect = !isDirect;
        printList();
    }

    @FXML
    void sortByIncome(ActionEvent event) {
        if (sortType != SortType.INCOME_SORT) {
            sortType = SortType.INCOME_SORT;
            isDirect = true;
        }

        companies.sort((n1, n2) -> (int) (n2.getAverageIncome() * 1000 - n1.getAverageIncome() * 1000) * (isDirect ? 1 : -1));
        isDirect = !isDirect;
        printList();
    }

    @FXML
    void sortByName(ActionEvent event) {
        if (sortType != SortType.NAME_SORT) {
            sortType = SortType.NAME_SORT;
            isDirect = true;
        }

        companies.sort((n1, n2) -> n2.getName().compareTo(n1.getName()) * (isDirect ? 1 : -1));
        isDirect = !isDirect;
        printList();
    }

    @FXML
    void getBack(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(pattern.getScene(), getClass(), "adminScene");
    }

    @FXML
    void update() throws IOException {
        Request request = new Request("companies");
        Response response = request.send();
        companies = new Gson().fromJson(response.getResponseBody(), listType);
        pattern.setText("");
        printList();
    }


    private void printList() {
        companiesList = FXCollections.observableArrayList(companies);
        nameColumn.setCellValueFactory(new PropertyValueFactory<CompanySerDes, String>("name"));
        amountColumn.setCellValueFactory(new PropertyValueFactory<CompanySerDes, Integer>("employeeAmount"));
        incomeColumn.setCellValueFactory(new PropertyValueFactory<CompanySerDes, Double>("averageIncome"));

        table.setItems(companiesList);

    }

    @FXML
    void initialize() throws IOException {
        update();
    }
}
