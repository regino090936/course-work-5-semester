package com.regino.client.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class CommonFunctions {
    public static void switchToNewWindow(Scene currentScene, Class currentClass, String newScene) throws IOException {
        Stage signStage = new Stage();
        signStage.setScene(new Scene(new FXMLLoader(currentClass.getResource("/views/" + newScene + ".fxml")).load()));

        currentScene.getWindow().hide();
        signStage.show();
    }

    public static void openNewWindow(Scene currentScene, Class currentClass, String newScene) throws IOException {
        Stage signStage = new Stage();
        signStage.setScene(new Scene(new FXMLLoader(currentClass.getResource("/views/" + newScene + ".fxml")).load()));
        signStage.show();
    }
}
