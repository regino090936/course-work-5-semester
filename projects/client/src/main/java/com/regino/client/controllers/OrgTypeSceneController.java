package com.regino.client.controllers;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.regino.client.connection.Request;
import com.regino.client.connection.Response;
import com.regino.client.connection.model.LocationSerDes;
import com.regino.client.connection.model.OrgTypeSerDes;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class OrgTypeSceneController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ListView<String> locationsList;

    @FXML
    private TextField pattern;

    @FXML
    private TextField newOrgType;

    @FXML
    private Text errorText;

    @FXML
    void addType(ActionEvent event) throws IOException {
        Request request = new Request("addType");
        if (newOrgType.getText().length() < 2 || !newOrgType.getText().matches(".*[a-z]+.*")) {
            errorText.setText("wrongFormat");
        }
        request.addAttribute("name", newOrgType.getText().toUpperCase());
        request.send();
        update();
    }

    @FXML
    void findLike(ActionEvent event) throws IOException {
        Request request = new Request("findLikeType");
        request.addAttribute("template", pattern.getText());
        Response response = request.send();
        types = new Gson().fromJson(response.getResponseBody(), listType);
        printList();

    }

    @FXML
    void getBack(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(errorText.getScene(), getClass(), "adminScene");
    }

    private List<OrgTypeSerDes> types;
    private Type listType = new TypeToken<List<OrgTypeSerDes>>() {}.getType();

    @FXML
    void update() throws IOException {
        Request request = new Request("orgTypes");
        Response response = request.send();
        types = new Gson().fromJson(response.getResponseBody(), listType);
        printList();
    }

    private void printList() {
        ObservableList<String> locs = FXCollections.observableArrayList();
        locs.addAll(types.stream()
                .map(OrgTypeSerDes::toString)
                .collect(Collectors.toList())
        );
        locationsList.setItems(locs);
    }

    @FXML
    void initialize() throws IOException {
        update();
    }
}
