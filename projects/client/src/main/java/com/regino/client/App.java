package com.regino.client;

import com.google.gson.Gson;
import com.regino.client.connection.Connection;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.net.URL;
import java.util.Arrays;

public class App extends Application {
    public static void main(String[] args){
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("9".matches("[0-9]"));
        System.out.println(new Gson().toJson(new String[] {"nujjj", "fjdksl", "jklj"}));
        System.out.println(new Gson().toJson(Arrays.asList("nujjj", "fjdksl", "jklj")));
        Connection.setConnection("localhost", 4444);
        Parent root = new FXMLLoader(getClass().getResource("/views/authScene.fxml")).load();

        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
