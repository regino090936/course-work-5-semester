package com.regino.client.connection.model;

public class LocationSerDes {
    private String country;
    private String city;

    public LocationSerDes(String country, String city) {
        this.country = country;
        this.city = city;
    }

    public LocationSerDes() {
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return String.format(
                "Country: %s, City: %s",
                this.country,
                this.city
        );
    }
}
