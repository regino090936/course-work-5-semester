package com.regino.client.controllers;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.ResourceBundle;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.regino.client.connection.Request;
import com.regino.client.connection.Response;
import com.regino.client.connection.model.LocationSerDes;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

public class LocationSceneController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private TableView<LocationSerDes> table;

    @FXML
    private TableColumn<LocationSerDes, String> country;

    @FXML
    private TableColumn<LocationSerDes, String> city;

    @FXML
    private TextField pattern;

    @FXML
    private TextField newAddress;

    @FXML
    private Text errorText;

    @FXML
    void addAddress(ActionEvent event) throws IOException {
        Request request = new Request("addAddress");
        String info = newAddress.getText().replaceAll(" ", "");
        request.addAttribute("country", info.split(",")[0]);
        request.addAttribute("city", info.split(",")[1]);
        request.addAttribute("company", info.split(",")[2]);
        request.send();
        update();
    }

    @FXML
    void findLike(ActionEvent event) throws IOException {
        Request request = new Request("findLikeAddress");
        String text = pattern.getText().replaceAll(" ", "");
        String[] strs = text.split(",");
        if (strs.length == 2) {
            request.addAttribute("country", strs[0]);
            request.addAttribute("city", strs[1]);
        } else if (strs.length == 1) {
            request.addAttribute("country", strs[0]);
        } else {
            errorText.setText("Неправильный формат");
            return;
        }
        Response response = request.send();
        Type itemsListType = new TypeToken<List<LocationSerDes>>() {}.getType();
        List<LocationSerDes> list = new Gson().fromJson(response.getResponseBody(), itemsListType);
        if (list.size() == 0) {
            errorText.setText("Совпадений не найдено");
            return;
        } else {
            errorText.setText("");
        }
        locations = list;
        printList();
    }

    @FXML
    void deleteAll(ActionEvent event) throws IOException {
        Request request = new Request("addressDelete");
        request.addAttribute("isAll", "true");
        request.send();
        update();
    }

    @FXML
    void deleteSelected(ActionEvent event) throws IOException {
        LocationSerDes location = table.getFocusModel().getFocusedItem();
        Request request = new Request("addressDelete");
        request.addAttribute("country", location.getCountry());
        request.addAttribute("city", location.getCity());
        request.send();
        update();
    }

    private boolean isDirect = true;
    private boolean isCountry = true;
    private List<LocationSerDes> locations;

    @FXML
    void sortCity(ActionEvent event) throws IOException {
        if (isCountry) {
            isDirect = true;
            isCountry = false;
        }

        if (isDirect) {
            locations.sort((n1, n2) -> n1.getCity().compareTo(n2.getCity()));
        } else {
            locations.sort((n1, n2) -> n2.getCity().compareTo(n1.getCity()));
        }
        isDirect = !isDirect;
        printList();
    }

    @FXML
    void sortCountry(ActionEvent event) throws IOException {
        if (!isCountry) {
            isDirect = true;
            isCountry = true;
        }

        if (isDirect) {
            locations.sort((n1, n2) -> n1.getCountry().compareTo(n2.getCountry()));
        } else {
            locations.sort((n1, n2) -> n2.getCountry().compareTo(n1.getCountry()));
        }
        isDirect = !isDirect;
        printList();
    }

    @FXML
    void getBack(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(pattern.getScene(), getClass(), "adminScene");
    }

    @FXML
    void update() throws IOException {
        Request request = new Request("addresses");
        Response response = request.send();
        Type itemsListType = new TypeToken<List<LocationSerDes>>() {}.getType();
        locations = new Gson().fromJson(response.getResponseBody(), itemsListType);
        printList();
        pattern.setText("");
        newAddress.setText("");
        errorText.setText("");
    }

    private void printList() {
        ObservableList<LocationSerDes> locs = FXCollections.observableArrayList(locations);
        country.setCellValueFactory(new PropertyValueFactory<LocationSerDes, String>("country"));
        city.setCellValueFactory(new PropertyValueFactory<LocationSerDes, String>("city"));
        table.setItems(locs);
    }

    @FXML
    void initialize() throws IOException {
        update();
    }
}
