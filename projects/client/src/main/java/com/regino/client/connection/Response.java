package com.regino.client.connection;

import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.stream.Stream;

public class Response {
    private Map<String, String> attrMap;
    private String jsonData;

    public Response(Map<String, String> attrMap, String jsonResponseBody) {
        this.attrMap = attrMap;
        this.jsonData = jsonResponseBody;
    }

    public void setAttrMap(Map<String, String> attrMap) {
        this.attrMap = attrMap;
    }

    public void setJsonResponseBody(String jsonResponseBody) {
        this.jsonData = jsonResponseBody;
    }

    public String getAttr(String name) {
        return attrMap.get(name);
    }

    public  Map<String, String> getAttrsMap() {
        return attrMap;
    }

    public Boolean isAttrMatch(String name, String value) {
        if (attrMap.get(name) == null)
            return value == null;
        return attrMap.get(name).equals(value);
    }
    public String getResponseBody() {
        return jsonData;
    }
}
