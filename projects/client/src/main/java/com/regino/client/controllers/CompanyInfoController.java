package com.regino.client.controllers;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.regino.client.connection.Request;
import com.regino.client.connection.Response;
import com.regino.client.connection.model.IncomeSerDes;
import com.regino.client.connection.model.LocationSerDes;
import com.regino.client.connection.model.UserSerDes;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

public class CompanyInfoController {

    @FXML
    private Text headerText;

    @FXML
    private TableView<LocationSerDes> tableLocation;

    @FXML
    private TableColumn<LocationSerDes, String> country;

    @FXML
    private TableColumn<LocationSerDes, String> city;

    @FXML
    private TableView<UserSerDes> tableEmployee;

    @FXML
    private TableColumn<UserSerDes, String> name;

    @FXML
    private TableColumn<UserSerDes, String> surname;

    @FXML
    private TableColumn<UserSerDes, String> mail;

    @FXML
    private TableColumn<UserSerDes, String> position;

    @FXML
    private TableView<UserSceneController.IncomeToTable> tableIncome;

    @FXML
    private TableColumn<UserSceneController.IncomeToTable, Integer> year;

    @FXML
    private TableColumn<UserSceneController.IncomeToTable, String> first;

    @FXML
    private TableColumn<UserSceneController.IncomeToTable, String> second;

    @FXML
    private TableColumn<UserSceneController.IncomeToTable, String> third;

    @FXML
    private TableColumn<UserSceneController.IncomeToTable, String> fourth;

    private static String companyName;

    public static String getCompanyName() {
        return companyName;
    }

    public static void setCompanyName(String companyName) {
        CompanyInfoController.companyName = companyName;
    }

    private final Type incomeType = new TypeToken<List<IncomeSerDes>>() {}.getType();
    private final Type userType = new TypeToken<List<UserSerDes>>() {}.getType();
    private final Type locationType = new TypeToken<List<LocationSerDes>>() {}.getType();

    @FXML
    void initialize() throws IOException {
        headerText.setText(companyName);

        ObservableList<UserSceneController.IncomeToTable> incomeList;
        Request request = new Request("getIncomes");
        request.addAttribute("name", companyName);
        Response response = request.send();
        List<IncomeSerDes> incomes = new Gson().fromJson(response.getResponseBody(), incomeType);
        incomeList = FXCollections.observableArrayList(incomes.stream().map(UserSceneController.IncomeToTable::new).collect(Collectors.toList()));
        year.setCellValueFactory(new PropertyValueFactory<UserSceneController.IncomeToTable, Integer>("year"));
        first.setCellValueFactory(new PropertyValueFactory<UserSceneController.IncomeToTable, String>("winter"));
        second.setCellValueFactory(new PropertyValueFactory<UserSceneController.IncomeToTable, String>("spring"));
        third.setCellValueFactory(new PropertyValueFactory<UserSceneController.IncomeToTable, String>("summer"));
        fourth.setCellValueFactory(new PropertyValueFactory<UserSceneController.IncomeToTable, String>("autumn"));
        tableIncome.setItems(incomeList);


        ObservableList<UserSerDes> usersList;
        request = new Request("users");
        request.addAttribute("companyName", companyName);
        response = request.send();
        List<UserSerDes> users = new Gson().fromJson(response.getResponseBody(), userType);
        usersList = FXCollections.observableArrayList(users);
        name.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("name"));
        surname.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("surname"));
        position.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("position"));
        mail.setCellValueFactory(new PropertyValueFactory<UserSerDes, String>("email"));
        tableEmployee.setItems(usersList);


        ObservableList<LocationSerDes> locationList;
        request = new Request("addresses");
        request.addAttribute("companyName", companyName);
        response = request.send();
        locationList = FXCollections.observableArrayList();
        List<LocationSerDes> locations = new Gson().fromJson(response.getResponseBody(), locationType);
        locationList = FXCollections.observableArrayList(locations);
        country.setCellValueFactory(new PropertyValueFactory<LocationSerDes, String>("country"));
        city.setCellValueFactory(new PropertyValueFactory<LocationSerDes, String>("city"));
        tableLocation.setItems(locationList);

    }
}
