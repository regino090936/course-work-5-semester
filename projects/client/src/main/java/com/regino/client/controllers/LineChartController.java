package com.regino.client.controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.regino.client.connection.model.IncomeSerDes;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

public class LineChartController {

    @FXML
    private LineChart<String, Number> chart;

    private static List<IncomeSerDes> incomes;
    private static IncomeSerDes forecastIncome;

    public static IncomeSerDes getForecastIncome() {
        return forecastIncome;
    }

    public static void setForecastIncome(IncomeSerDes forecastIncome) {
        LineChartController.forecastIncome = forecastIncome;
        System.out.println(forecastIncome);
    }

    public static List<IncomeSerDes> getIncomes() {
        return incomes;
    }

    public static void setIncomes(List<IncomeSerDes> incomes) {
        LineChartController.incomes = incomes;
    }

    @FXML
    void initialize() {
        chart.getData().clear();
        XYChart.Series<String, Number> series = new XYChart.Series<>();
        for (IncomeSerDes income : incomes) {
            series.getData().add(new XYChart.Data<String, Number>(Integer.toString(income.getYear()), income.getSum()));
        }
        System.out.println(Integer.toString(forecastIncome.getYear()) + ", " + forecastIncome.getSum());
        series.getData().add(new XYChart.Data<String, Number>(Integer.toString(forecastIncome.getYear()), forecastIncome.getSum()));
        chart.getData().add(series);



    }
}
