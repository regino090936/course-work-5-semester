package com.regino.client.controllers;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.regino.client.connection.Request;
import com.regino.client.connection.Response;
import com.regino.client.connection.model.OrgTypeSerDes;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class NewCompanySceneController {

    @FXML
    private TextField companyName;

    @FXML
    private ListView<String> orgList;

    @FXML
    void submit(ActionEvent event) throws IOException {
        Request request = new Request("addCompany");
        if (!companyName.getText().matches(".*[a-z]+.*"))
            return;
        request.addAttribute("name", companyName.getText());
        request.addAttribute("orgType", orgList.getFocusModel().getFocusedItem());
        request.send();
        getback();
    }

    @FXML
    void initialize() throws IOException {
        Request request = new Request("orgTypes");
        Response response = request.send();
        List<OrgTypeSerDes> types = new Gson().fromJson(response.getResponseBody(), new TypeToken<List<OrgTypeSerDes>>() {}.getType());
        ObservableList<String> typesObs = FXCollections.observableArrayList();
        typesObs.addAll(types.stream()
                .map(OrgTypeSerDes::toString)
                .collect(Collectors.toList())
        );
        orgList.setItems(typesObs);
    }

    public void getback() throws IOException {
        CommonFunctions.switchToNewWindow(companyName.getScene(), getClass(), "CompaniesScene");
    }
}
