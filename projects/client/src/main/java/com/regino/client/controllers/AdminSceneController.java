package com.regino.client.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.regino.client.connection.Request;
import com.regino.client.connection.Response;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class AdminSceneController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button updateBtn;

    @FXML
    private Text ping;

    @FXML
    private Text connectionCount;

    @FXML
    private ListView<String> connectionList;

    @FXML
    private Text connCount;

    @FXML
    void getBack(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(connCount.getScene(), getClass(), "authScene");
    }

    @FXML
    void selectAddresses(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(connCount.getScene(), getClass(), "AddressesScene");
    }

    @FXML
    void selectCompany(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(connCount.getScene(), getClass(), "CompaniesScene");
    }

    @FXML
    void selectTypeOrg(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(connCount.getScene(), getClass(), "OrgTypesScene");
    }

    @FXML
    void selectUsers(ActionEvent event) throws IOException {
        CommonFunctions.switchToNewWindow(connCount.getScene(), getClass(), "UsersScene");
    }

    @FXML
    void update(ActionEvent event) throws IOException {
        initialize();
    }

    @FXML
    void initialize() throws IOException {
        Request emptyRequest = new Request("empty");
        Long time1 = System.currentTimeMillis();
        emptyRequest.send();
        Long time2 = System.currentTimeMillis();
        System.out.println(time1);
        System.out.println(time2);
        ping.setText(String.valueOf(time2 - time1) + " ms");

        Response response = new Request("activeUsers").send();

        Type usersType = new TypeToken<List<String>>() {}.getType();
        List<String> users = new Gson().fromJson(response.getResponseBody(), usersType);

        ObservableList<String> usrs = FXCollections.observableArrayList();
        usrs.addAll(users);
        connectionList.setItems(usrs);
    }
}
