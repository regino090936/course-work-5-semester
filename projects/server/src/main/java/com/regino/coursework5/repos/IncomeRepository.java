package com.regino.coursework5.repos;

import com.regino.coursework5.entity.Income;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IncomeRepository extends CrudRepository<Income, Integer> {
}
