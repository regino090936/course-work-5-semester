package com.regino.coursework5.entity.serializable;

import com.regino.coursework5.entity.Location;

public class LocationSerDes {
    private String country;
    private String city;

    public LocationSerDes(String country, String city) {
        this.country = country;
        this.city = city;
    }

    public LocationSerDes() {
    }

    public LocationSerDes(Location location) {
        this.country = location.getCountry();
        this.city = location.getCity();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
