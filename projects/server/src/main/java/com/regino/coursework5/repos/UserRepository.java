package com.regino.coursework5.repos;

import com.regino.coursework5.entity.User;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public interface UserRepository extends CrudRepository<User, Integer> {
    public Optional<User> findUserByLogin(String login);
    public List<User> findAllByNameStartsWith(String s);
    public List<User> findAllBySurnameStartsWith(String s);
    public List<User> findAllByEmailStartsWith(String s);
    public List<User> findAllByPositionStartsWith(String s);
    public List<User> findAllByLoginStartsWith(String s);
    public List<User> findAllByCompany_CompanyNameStartsWith(String s);
    public List<User> findAllByCompany_CompanyName(String s);
    public List<User> findAllByLocation_CityStartsWith(String s);
    public List<User> findAllByLocation_CountryStartsWith(String s);
    public void deleteAllByLogin(String s);
}
