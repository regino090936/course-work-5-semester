package com.regino.coursework5.controllers;

import com.regino.coursework5.entity.Company;
import com.regino.coursework5.entity.Income;
import com.regino.coursework5.repos.CompanyRepository;
import com.regino.coursework5.repos.LocationRepository;
import com.regino.coursework5.repos.OrgTypeRepository;
import com.regino.coursework5.repos.UserRepository;
import com.regino.coursework5.server.data.Request;
import com.regino.coursework5.server.data.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Component
public class UserController {
    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final LocationRepository locationRepository;
    private final OrgTypeRepository orgTypesRepository;

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public UserController(UserRepository userRepository, CompanyRepository companyRepository, LocationRepository locationRepository, OrgTypeRepository orgTypesRepository) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.locationRepository = locationRepository;
        this.orgTypesRepository = orgTypesRepository;
    }

    @Transactional
    public void editIncome(Request request, Response response) {
        Optional<Company> companyOptional = companyRepository.findCompanyByCompanyName(request.getAttribute("companyName"));
        if(companyOptional.isEmpty()) {
            response.addAttribute("isCompanyExist", "false");
            return;
        }
        int year = Integer.parseInt(request.getAttribute("year"));

        Optional<Income> incomeOptional = companyOptional.get().getIncomes().stream()
            .filter(n -> n.getYear() == year)
            .findAny();

        if (incomeOptional.isPresent()) {
            if (request.getAttribute("isReplace").equals("true")) {
                incomeOptional.get().setWinterIncome(Double.parseDouble(request.getAttribute("1")));
                incomeOptional.get().setSpringIncome(Double.parseDouble(request.getAttribute("2")));
                incomeOptional.get().setSummerIncome(Double.parseDouble(request.getAttribute("3")));
                incomeOptional.get().setAutumnIncome(Double.parseDouble(request.getAttribute("4")));
            } else {
                incomeOptional.get().setWinterIncome(incomeOptional.get().getWinterIncome() + Double.parseDouble(request.getAttribute("1")));
                incomeOptional.get().setSpringIncome(incomeOptional.get().getSpringIncome() + Double.parseDouble(request.getAttribute("2")));
                incomeOptional.get().setSummerIncome(incomeOptional.get().getSummerIncome() + Double.parseDouble(request.getAttribute("3")));
                incomeOptional.get().setAutumnIncome(incomeOptional.get().getAutumnIncome() + Double.parseDouble(request.getAttribute("4")));
            }
        } else {
            Income income = new Income();
            income.setYear(year);
            income.setWinterIncome(Double.parseDouble(request.getAttribute("1")));
            income.setSpringIncome(Double.parseDouble(request.getAttribute("2")));
            income.setSummerIncome(Double.parseDouble(request.getAttribute("3")));
            income.setAutumnIncome(Double.parseDouble(request.getAttribute("4")));
            companyOptional.get().addIncome(income);
        }
    }

    private class Forecast {
        public double q1;
    }

    @Transactional
    public void makeForecast(Request request, Response response) {
        Optional<Company> companyOptional = companyRepository.findCompanyByCompanyName(request.getAttribute("companyName"));
        if(companyOptional.isEmpty()) {
            response.addAttribute("isCompanyExist", "false");
            return;
        }
        int yearBegin = Integer.parseInt(request.getAttribute("yearBegin"));
        StringBuilder report = new StringBuilder();

        report.append("Sum Incomes: ");
        List<Income> incomes = companyOptional.get().getIncomes().stream().filter(n -> n.getYear() >= yearBegin).collect(Collectors.toList());
        List<Income> Ii = incomes.stream()
                .map(n -> new Income(n.getYear(), n.getWinterIncome(), n.getSpringIncome(), n.getSummerIncome(), n.getAutumnIncome()))
                .sorted(Comparator.comparingInt(Income::getYear))
                .peek(n -> {
                    double sum = n.getSumIncome();
                    report.append(sum).append(", ");
                    n.setWinterIncome(n.getWinterIncome() / (sum / 4));
                    n.setSpringIncome(n.getSpringIncome() / (sum / 4));
                    n.setSummerIncome(n.getSummerIncome() / (sum / 4));
                    n.setAutumnIncome(n.getAutumnIncome() / (sum / 4));
                })
                .collect(Collectors.toList());

        report.append("\n\n");
        System.out.println( Integer.toString(Ii.get(Ii.size() - 1).getYear() + 1));
        response.addAttribute("year", Integer.toString(Ii.get(Ii.size() - 1).getYear() + 1));
        double[] CpIi = new double[4];
        CpIi[0] = Ii.stream().mapToDouble(Income::getWinterIncome).average().getAsDouble();
        report.append("Average index for winter: ").append(CpIi[0]).append('\n');
        CpIi[1] = Ii.stream().mapToDouble(Income::getSpringIncome).average().getAsDouble();
        report.append("Average index for spring: ").append(CpIi[1]).append('\n');
        CpIi[2] = Ii.stream().mapToDouble(Income::getSummerIncome).average().getAsDouble();
        report.append("Average index for summer: ").append(CpIi[2]).append('\n');
        CpIi[3] = Ii.stream().mapToDouble(Income::getAutumnIncome).average().getAsDouble();
        report.append("Average index for autumn: ").append(CpIi[3]).append('\n');

        double Ic = Math.pow(incomes.get(Ii.size() - 1).getSumIncome() / incomes.get(0).getSumIncome(), 1 / ((double) Ii.size() - 1));
        double Ynk = incomes.get(Ii.size() - 1).getSumIncome() * Ic;
        double Ydnk = incomes.get(Ii.size() - 1).getSumIncome() + (incomes.get(Ii.size() - 1).getSumIncome() - incomes.get(0).getSumIncome()) / (Ii.size() - 1);
        double result = (Ynk + Ydnk) / 2;
        report.append("\nIncome forecast: ").append(result).append("\n\n");

        response.addAttribute("1", Double.toString(result * CpIi[0]));
        report.append("Income forecast for winter: ").append(result / 4 * CpIi[0]).append('\n');
        response.addAttribute("2", Double.toString(result  / 4 * CpIi[1]));
        report.append("Income forecast for spring: ").append(result / 4 * CpIi[1]).append('\n');
        response.addAttribute("3", Double.toString(result / 4 * CpIi[2]));
        report.append("Income forecast for summer: ").append(result / 4 * CpIi[2]).append('\n');
        response.addAttribute("4", Double.toString(result / 4 * CpIi[3]));
        report.append("Income forecast for autumn: ").append(result / 4 * CpIi[3]).append('\n');

        response.addAttribute("report", report.toString());
    }
}
