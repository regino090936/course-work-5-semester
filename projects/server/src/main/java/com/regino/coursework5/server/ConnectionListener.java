package com.regino.coursework5.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class ConnectionListener extends Thread{

    private final ServerSocket serverSocket;
    private final Logger logger = LoggerFactory.getLogger(ConnectionListener.class);

    private static final Set<ConnectionHandler> connections = new HashSet<>();

    public ConnectionListener(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    public static Set<ConnectionHandler> getConnections() {
        return connections;
    }

    @Override
    public void run() {
        Socket socket;
        try {
            while (!isInterrupted()) {
                socket = serverSocket.accept();
                connections.add(
                        new ConnectionHandler(socket)
                );
                logger.info("Connection accepted");
            }
        } catch (IOException e){
            logger.error("accepting error");
        }
    }
}
