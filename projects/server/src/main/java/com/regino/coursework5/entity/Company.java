package com.regino.coursework5.entity;

import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Entity
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer companyId;

    private String companyName;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "company")
    private List<User> users = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JoinTable(
            name = "company_locations",
            joinColumns = { @JoinColumn(name = "company_id")},
            inverseJoinColumns = {@JoinColumn(name = "location_id")}
    )
    private List<Location> locations = new LinkedList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id")
    private List<Income> incomes = new ArrayList<>();

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "orgtype_id")
    private OrgType orgType;

    @PreRemove
    public void removeCompanyFromLocations() {
        this.setOrgType(null);

        for (User u : users) {
            u.setCompany(null);
            u.setLocation(null);
        }
        for (Location l : locations) {
            l.getCompanies().remove(this);
        }
    }


    public Company(String companyName) {
        this.companyName = companyName;
    }

    public Company() {
    }

    public OrgType getOrgType() {
        return orgType;
    }

    public void setOrgType(OrgType orgType) {
        this.orgType = orgType;
    }

    public List<Income> getIncomes() {
        return incomes;
    }

    public void setIncomes(List<Income> incomes) {
        this.incomes = incomes;
    }

    public void addIncome(Income income) {
        this.incomes.add(income);
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
        users.forEach((n) -> n.setCompany(this));
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public void addLocation(Location location) {
        if (!locations.contains(location)) {
            locations.add(location);
        }
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("company - ").append(this.companyName).append('\n');
        str.append("organisation type: ").append(orgType).append('\n');
        if (users != null) {
            str.append("employee:\n");
            this.users.forEach((n) -> str.append("  ").append(n).append('\n'));
        }
        if (incomes != null) {
            str.append("income:\n");
            this.incomes.forEach((n) -> str.append("  ").append(n).append('\n'));
        }
        if (locations != null) {
            str.append("locations:\n");
            this.locations.forEach((n) -> str.append("  ").append(n).append('\n'));
        }

        return str.toString();
    }
}
