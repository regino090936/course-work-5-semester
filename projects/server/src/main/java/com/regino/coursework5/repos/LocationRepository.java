package com.regino.coursework5.repos;

import com.regino.coursework5.entity.Location;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public interface LocationRepository extends CrudRepository<Location, Integer> {
    public void deleteAllByCityAndCountry(String city, String country);
    public Optional<Location> findLocationByCityAndCountry(String city, String country);
    public List<Location> findAllByCountryAndCityStartsWith(String country, String city);
    public List<Location> findAllByCountry(String country);
}
