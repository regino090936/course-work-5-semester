package com.regino.coursework5.runners;

import com.regino.coursework5.entity.*;
import com.regino.coursework5.repos.CompanyRepository;
import com.regino.coursework5.repos.LocationRepository;
import com.regino.coursework5.repos.OrgTypeRepository;
import com.regino.coursework5.repos.UserRepository;
import com.regino.coursework5.server.ConnectionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.*;

@Component
public class ServerRunner implements CommandLineRunner {

    private final ConnectionListener connectionListener;

    private final CompanyRepository companyRepository;
    private final UserRepository userRepository;
    private final LocationRepository locationRepository;
    private final OrgTypeRepository orgTypeRepository;

    private final Logger logger = LoggerFactory.getLogger(ServerRunner.class);

    public ServerRunner(ConnectionListener connectionListener, CompanyRepository companyRepository, UserRepository userRepository, LocationRepository locationRepository, OrgTypeRepository orgTypeRepository) {
        this.connectionListener = connectionListener;
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
        this.locationRepository = locationRepository;
        this.orgTypeRepository = orgTypeRepository;
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        OrgType orgType = new OrgType("ODO");
        orgTypeRepository.save(orgType);
        connectionListener.start();
        logger.info("Start connectionListener");

        for (int i = 0; i < 3; ++i) {
            Company company = new Company("name" + i);

            List<Location> locations = List.of(
                    new Location("cn" + i + ".1", "ct" + i + ".1"),
                    new Location("cntry" + i + ".2", "city" + i + ".2"),
                    new Location("cntry" + i + ".3", "city" + i + ".3"),
                    new Location("cntry" + i + ".4", "city" + i + ".4")
            );

            locationRepository.saveAll(locations);
            company.setLocations(locations);

            List<User> users = new LinkedList<>();
            for (int j = 0; j < 2 + (int) (Math.random() * 7); ++j) {
                User user = new User("usn" + i + "." + j, "uss" + i + "." + j);
                user.setLogin("l" + i + "" + j);
                user.setPassword("p");
                user.setEmail("email");
                user.setAdmin(j == 0 ? "true" : "false");
                user.setPosition("pos");
                user.setLocation(locations.get((int) (Math.random() * locations.size())));
                users.add(user);
            }

            company.setUsers(users);

            company.setIncomes(List.of(
                    new Income(2018, 1 + Math.random() * 10, 1 + Math.random() * 10, 1 + Math.random() * 10,1 + Math.random() * 10),
                    new Income(2019, 2 + Math.random() * 10, 2 + Math.random() * 10, 2 + Math.random() * 10, 2 + Math.random() * 10),
                    new Income(2020, 4 + Math.random() * 10, 4 + Math.random() * 10, 4 + Math.random() * 10, 4 + Math.random() * 10)
            ));

            List<OrgType> list = new ArrayList<>();
            orgTypeRepository.findAll().forEach(list::add);
            company.setOrgType(list.get(0));
            companyRepository.save(company);
        }

        companyRepository.findAll().forEach(System.out::println);

    }
}
