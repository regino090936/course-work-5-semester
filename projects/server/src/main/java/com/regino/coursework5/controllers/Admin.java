package com.regino.coursework5.controllers;

import com.regino.coursework5.entity.*;
import com.regino.coursework5.entity.serializable.*;
import com.regino.coursework5.repos.CompanyRepository;
import com.regino.coursework5.repos.LocationRepository;
import com.regino.coursework5.repos.OrgTypeRepository;
import com.regino.coursework5.repos.UserRepository;
import com.regino.coursework5.server.ConnectionHandler;
import com.regino.coursework5.server.ConnectionListener;
import com.regino.coursework5.server.data.Request;
import com.regino.coursework5.server.data.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

@Component
public class Admin {
    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final LocationRepository locationRepository;
    private final OrgTypeRepository orgTypesRepository;
    private final Set<ConnectionHandler> connections = ConnectionListener.getConnections();
    private final Logger logger = LoggerFactory.getLogger(Admin.class);

    @Autowired
    public Admin(UserRepository userRepository, CompanyRepository companyRepository, LocationRepository locationRepository, OrgTypeRepository orgTypesRepository) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.locationRepository = locationRepository;
        this.orgTypesRepository = orgTypesRepository;
    }

    public void emptyRequest(Request request, Response response) {
    }

    public void getUsers(Request request, Response response) {
        Set<UserSerDes> users = new HashSet<>();
        if (request.getAttribute("companyName") == null)
            userRepository.findAll().forEach(n -> users.add(new UserSerDes(n)));
        else
            userRepository.findAllByCompany_CompanyName(request.getAttribute("companyName")).forEach(n -> users.add(new UserSerDes(n)));
        response.setData(users);
    }

    public void getActiveUsers(Request request, Response response) {
        List<String> activeUsers = connections.stream()
                .map(n -> n.getLogin() + ", " + n.getStatus())
                .collect(Collectors.toList());
        response.setData(activeUsers);
    }

    @Transactional
    public void getAddresses(Request request, Response response) {
        List<LocationSerDes> locations = new LinkedList<>();
        if (request.getAttribute("companyName") == null) {
            List<LocationSerDes> finalLocations = locations;
            locationRepository.findAll().forEach(n -> {
                finalLocations.add(new LocationSerDes(n.getCountry(), n.getCity()));
            });
            locations = finalLocations;
        }
        else
            locations = companyRepository.findCompanyByCompanyName(request.getAttribute("companyName")).get().getLocations()
                    .stream()
                    .map(LocationSerDes::new)
                    .collect(Collectors.toList());

        response.setData(locations);
    }

    public void getOrgTypes(Request request, Response response) {
        List<OrgTypeSerdes> orgTypes = new LinkedList<>();
        orgTypesRepository.findAll().forEach(n -> {
            orgTypes.add(new OrgTypeSerdes(n.getType()));
        });

        response.setData(orgTypes);
    }

    public void filtrUsers(Request request, Response response) {
        String value = request.getAttribute("value");
        List<User> users = null;
        switch (request.getAttribute("filtrField")) {
            case "name" -> users = userRepository.findAllByNameStartsWith(value);
            case "surname" -> users = userRepository.findAllBySurnameStartsWith(value);
            case "email" -> users = userRepository.findAllByEmailStartsWith(value);
            case "position" -> users = userRepository.findAllByPositionStartsWith(value);
            case "login" -> users = userRepository.findAllByLoginStartsWith(value);
            case "city" -> users = userRepository.findAllByLocation_CityStartsWith(value);
            case "country" -> users = userRepository.findAllByLocation_CountryStartsWith(value);
            case "company" -> users = userRepository.findAllByCompany_CompanyNameStartsWith(value);
            case "default" -> logger.warn("wrong filter format");
        }
        if (users == null)
            return;
        response.setData(
                users.stream()
                    .map(UserSerDes::new)
                    .collect(Collectors.toList())
        );
    }

    @Transactional
    public void deleteUser(Request request, Response response) {
        userRepository.deleteAllByLogin(request.getAttribute("login"));
    }

    public void deleteAddress(Request request, Response response) {
        if (request.getAttribute("isAll") != null) {
            if (request.getAttribute("isAll").equals("true"))
                locationRepository.deleteAll();
        } else {
            locationRepository.deleteAllByCityAndCountry(
                    request.getAttribute("city"),
                    request.getAttribute("country")
            );
        }
    }

    @Transactional
    public void addAddress(Request request, Response response) {
        Location location = new Location();
        Optional<Company> companyOptional = companyRepository.findCompanyByCompanyName(request.getAttribute("company"));
        String city = request.getAttribute("city");
        String country = request.getAttribute("country");
        if (companyOptional.isEmpty() || city == null || country == null) {
            logger.warn("cannot add location");
            return;
        }
        location.setCity(request.getAttribute("city"));
        location.setCountry(request.getAttribute("country"));
        location.addCompany(companyOptional.get());
        locationRepository.save(location);
    }

    public void addType(Request request, Response response) {
        Optional<OrgType> orgTypeOptional = orgTypesRepository.findOrgTypeByType(request.getAttribute("name"));
        if (orgTypeOptional.isPresent() || request.getAttribute("name").length() == 0 )
            return;
        orgTypesRepository.save(new OrgType(request.getAttribute("name")));
    }

    public void findLikeAddress(Request request, Response response) {
        List<Location> locations;
        if (request.getAttribute("city") == null) {
            locations = locationRepository.findAllByCountry(request.getAttribute("country"));
        } else {
            locations = locationRepository.findAllByCountryAndCityStartsWith(
                    request.getAttribute("country"),
                    request.getAttribute("city")
            );
        }
        response.setData(
                locations.stream()
                        .map(n -> new LocationSerDes(n.getCountry(), n.getCity()))
                        .collect(Collectors.toList())
        );
    }

    public void findLikeType(Request request, Response response) {
        List<OrgType> types = orgTypesRepository.findAllByTypeStartsWith(request.getAttribute("template").toUpperCase());
        response.setData(
                types.stream()
                    .map(n -> new OrgTypeSerdes(n.getType()))
                    .collect(Collectors.toList())
        );
    }

    @Transactional
    public void getCompanies(Request request, Response response) {
        response.setData(getSerDes(companyRepository.findAll()));
    }

    @Transactional
    List<CompanySerDes> getSerDes(Iterable<Company> companies) {
        List<CompanySerDes> companiesSerDes = new LinkedList<>();
        companies.forEach(c -> {
            companiesSerDes.add(new CompanySerDes(
                    c.getCompanyName(),
                    c.getUsers().size(),
                    c.getIncomes().size() != 0
                            ? (c.getIncomes().stream()
                            .flatMapToDouble(n -> DoubleStream.of(n.getAutumnIncome(), n.getSpringIncome(), n.getWinterIncome(), n.getSummerIncome()))
                            .sum() / c.getIncomes().size())
                            : 0
            ));
        });
        return companiesSerDes;
    }

    @Transactional
    public void findCompanies(Request request, Response response) {
        response.setData(
                getSerDes(companyRepository.findAllByCompanyNameStartsWith(request.getAttribute("value")))
        );
    }

    @Transactional
    public void deleteCompany(Request request, Response response) {
        logger.info("deleting company");
        companyRepository.findAll();
        companyRepository.removeCompanyByCompanyName("name1");
    }

    @Transactional
    public void getIncomes(Request request, Response response) {
        Optional<Company> companyOptional = companyRepository.findCompanyByCompanyName(request.getAttribute("name"));
        if (companyOptional.isEmpty())
            return;

        response.setData(
                companyOptional.get().getIncomes()
                        .stream()
                        .map(IncomeSerDes::new)
                        .collect(Collectors.toList())
        );
    }

    @Transactional
    public void addCompany(Request request, Response response) {
        Company company = new Company();
        company.setOrgType(orgTypesRepository.findOrgTypeByType(request.getAttribute("orgType")).get());
        company.setCompanyName(request.getAttribute("name"));
        companyRepository.save(company);
    }
}
