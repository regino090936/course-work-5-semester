package com.regino.coursework5.repos;

import com.regino.coursework5.entity.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface CompanyRepository extends CrudRepository<Company,Integer> {
    public Optional<Company> findCompanyByCompanyName(String companyName);
    public List<Company> findAllByCompanyNameStartsWith(String template);
    public void removeCompanyByCompanyName(String name);
}
