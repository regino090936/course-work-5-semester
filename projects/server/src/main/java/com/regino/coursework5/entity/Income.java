package com.regino.coursework5.entity;

import javax.persistence.*;

@Entity
public class Income {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer Id;

    private int year;
    private double winterIncome;
    private double springIncome;
    private double summerIncome;
    private double autumnIncome;


    public Income(int year, double winterIncome, double springIncome, double summerIncome, double outemnIncome) {
        this.year = year;
        this.winterIncome = winterIncome;
        this.springIncome = springIncome;
        this.summerIncome = summerIncome;
        this.autumnIncome = outemnIncome;
    }

    public double getWinterIncome() {
        return winterIncome;
    }

    public void setWinterIncome(double winterIncome) {
        this.winterIncome = winterIncome;
    }

    public double getSpringIncome() {
        return springIncome;
    }

    public void setSpringIncome(double springIncome) {
        this.springIncome = springIncome;
    }

    public double getSummerIncome() {
        return summerIncome;
    }

    public void setSummerIncome(double summerIncome) {
        this.summerIncome = summerIncome;
    }

    public double getAutumnIncome() {
        return autumnIncome;
    }

    public void setAutumnIncome(double outemnIncome) {
        this.autumnIncome = outemnIncome;
    }

    public Income() {
    }

    public double getSumIncome() {
        return winterIncome + springIncome + summerIncome + autumnIncome;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Income{" +
                "year=" + year +
                ", winterIncome=" + winterIncome +
                ", springIncome=" + springIncome +
                ", summerIncome=" + summerIncome +
                ", outemnIncome=" + autumnIncome +
                '}';
    }
}
