package com.regino.coursework5.server;

import com.google.gson.Gson;
import com.regino.coursework5.controllers.Admin;
import com.regino.coursework5.controllers.Authentication;
import com.regino.coursework5.controllers.SpringUtils;
import com.regino.coursework5.controllers.UserController;
import com.regino.coursework5.server.data.Request;
import com.regino.coursework5.server.data.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class RequestDispatcher {
    private static final Logger logger = LoggerFactory.getLogger(RequestDispatcher.class);
    private static final Map<String, Method> methods = new HashMap<>();
    private static final Gson gson = new Gson();

    static {
        try {
            methods.put("auth", Authentication.class.getDeclaredMethod("authorization", Request.class, Response.class));
            methods.put("signup", Authentication.class.getDeclaredMethod("registration", Request.class, Response.class));
            methods.put("empty", Admin.class.getDeclaredMethod("emptyRequest", Request.class, Response.class));
            methods.put("users", Admin.class.getDeclaredMethod("getUsers", Request.class, Response.class));
            methods.put("activeUsers", Admin.class.getDeclaredMethod("getActiveUsers", Request.class, Response.class));
            methods.put("addresses", Admin.class.getDeclaredMethod("getAddresses", Request.class, Response.class));
            methods.put("addressDelete", Admin.class.getDeclaredMethod("deleteAddress", Request.class, Response.class));
            methods.put("addAddress", Admin.class.getDeclaredMethod("addAddress", Request.class, Response.class));
            methods.put("findLikeAddress", Admin.class.getDeclaredMethod("findLikeAddress", Request.class, Response.class));
            methods.put("orgTypes", Admin.class.getDeclaredMethod("getOrgTypes", Request.class, Response.class));
            methods.put("addType", Admin.class.getDeclaredMethod("addType", Request.class, Response.class));
            methods.put("findLikeType", Admin.class.getDeclaredMethod("findLikeType", Request.class, Response.class));
            methods.put("filtrUsers", Admin.class.getDeclaredMethod("filtrUsers", Request.class, Response.class));
            methods.put("userDelete", Admin.class.getDeclaredMethod("deleteUser", Request.class, Response.class));
            methods.put("companies", Admin.class.getDeclaredMethod("getCompanies", Request.class, Response.class));
            methods.put("findCompany", Admin.class.getDeclaredMethod("findCompanies", Request.class, Response.class));
            methods.put("deleteCompany", Admin.class.getDeclaredMethod("deleteCompany", Request.class, Response.class));
            methods.put("getIncomes", Admin.class.getDeclaredMethod("getIncomes", Request.class, Response.class));
            methods.put("addCompany", Admin.class.getDeclaredMethod("addCompany", Request.class, Response.class));
            methods.put("editIncome", UserController.class.getDeclaredMethod("editIncome", Request.class, Response.class));
            methods.put("forecast", UserController.class.getDeclaredMethod("makeForecast", Request.class, Response.class));
        } catch (NoSuchMethodException e) {
            logger.warn("cannot find declared method while mapping requests");
        }
    }

    public static String executeRequest(String jsonMessage) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Request request = gson.fromJson(jsonMessage, Request.class);
        Method method = methods.get(request.getRequestType());
        method.setAccessible(true);
        Response response = new Response();

        method.invoke(
                SpringUtils.getBean(method.getDeclaringClass()),
                request,
                response
        );

        return gson.toJson(response);
    }
}
