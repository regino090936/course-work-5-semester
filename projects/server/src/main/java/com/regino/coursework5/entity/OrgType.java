package com.regino.coursework5.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class OrgType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String type;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orgType", fetch = FetchType.EAGER)
    private Set<Company> companies;

    public OrgType(String type) {
        this.type = type;
    }

    public OrgType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
