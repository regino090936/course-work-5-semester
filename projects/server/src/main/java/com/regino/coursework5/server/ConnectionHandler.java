package com.regino.coursework5.server;

import com.google.gson.Gson;
import com.regino.coursework5.server.data.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.net.SocketException;
import java.util.Map;

public class ConnectionHandler extends Thread {
    private final Logger logger = LoggerFactory.getLogger(ConnectionHandler.class);

    private final Socket socket;
    private final BufferedReader in;
    private final PrintWriter out;

    private String login = null;
    private String status = null;

    public ConnectionHandler(Socket socket) throws IOException {
        this.socket = socket;
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.out = new PrintWriter(new BufferedOutputStream(socket.getOutputStream()));
        logger.info("New connection registered");
        this.start();
    }

    private final static Gson gson = new Gson();

    @Override
    public void run() {
        try {
            while (!isInterrupted()) {
                try {
                    String msg = in.readLine();
                    logger.info("receive request - " + msg);
                    String responseJson = RequestDispatcher.executeRequest(msg);

                    Response response = gson.fromJson(responseJson, Response.class);
                    Map<String, String> map = response.getAttrMap();
                    String name = map.remove("clientLogin");
                    String status = map.remove("clientStatus");
                    if (!(name == null || status == null))
                        logger.info("name = " + name + ", status = " + status);
                    responseJson = gson.toJson(response);
                    this.login = name == null ? this.login : name;
                    this.status = status == null ? this.status : (status.equals("true") ? "admin" : "user");

                    out.println(responseJson);
                    out.flush();
                } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
                    logger.warn("connection closed");
                    this.interrupt();
                } catch (SocketException e) {
                    logger.error(e.getMessage());
                    socket.close();
                    this.interrupt();
                }
            }

        } catch (IOException e) {
            logger.warn("Exception while closing connection");
        }

        ConnectionListener.getConnections().remove(this);
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLogin() {
        return login;
    }

    public String getStatus() {
        return status;
    }
}
