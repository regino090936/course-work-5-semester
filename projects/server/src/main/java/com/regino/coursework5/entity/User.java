package com.regino.coursework5.entity;

import com.regino.coursework5.entity.serializable.UserSerDes;

import javax.persistence.*;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "employee_company_id")
    private Company company;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "location_id")
    private Location location;

    private String name;
    private String surname;
    private String email;
    private String position;
    private String isAdmin;

    private String login;
    private String password;

    public User(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public User(UserSerDes user, String password, Company company, Location location) {
        this.name = user.getName();
        this.surname = user.getSurname();
        this.email = user.getEmail();
        this.position = user.getPosition();
        this.isAdmin = user.getStatus().equals("admin") ? "true" : "false";
        this.login = user.getLogin();
        this.password = password;
        this.company = company;
        this.location = location;
    }

    public User() {
    }

    public String getAdmin() {
        return isAdmin;
    }

    public void setAdmin(String admin) {
        isAdmin = admin;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "User{ name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
