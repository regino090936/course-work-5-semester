package com.regino.coursework5.server.data;

import java.util.HashMap;
import java.util.Map;

public class Request {
    private final String requestType;
    private final Map<String, String> attrMap;
    private String body;

    public Request(String requestType, Map<String, String> attrMap) {
        this.requestType = requestType;
        this.attrMap = attrMap;
    }



    public String getAttribute(String name) {
        return attrMap.get(name);
    }

    public String getRequestType() {
        return requestType;
    }

    public String getBody() {
        return body;
    }
}
