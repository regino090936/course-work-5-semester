package com.regino.coursework5.entity.serializable;

import com.regino.coursework5.entity.User;

public class UserSerDes {

    private String companyName;
    private String country;
    private String city;
    private String name;
    private String surname;
    private String email;
    private String position;
    private String login;
    private String status;


    public UserSerDes(User user) {
        companyName = user.getCompany().getCompanyName();
        country = user.getLocation().getCountry();
        city = user.getLocation().getCity();
        name = user.getName();
        surname = user.getSurname();
        email = user.getEmail();
        position = user.getPosition();
        login = user.getLogin();
        status = user.getAdmin().equals("true") ? "Admin" : "User";
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPosition() {
        return position;
    }

    public String getLogin() {
        return login;
    }

    public String getStatus() {
        return status;
    }
}
