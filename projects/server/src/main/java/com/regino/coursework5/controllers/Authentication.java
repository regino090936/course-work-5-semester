package com.regino.coursework5.controllers;

import com.google.gson.Gson;
import com.regino.coursework5.entity.Company;
import com.regino.coursework5.entity.Location;
import com.regino.coursework5.entity.User;
import com.regino.coursework5.entity.serializable.UserSerDes;
import com.regino.coursework5.repos.CompanyRepository;
import com.regino.coursework5.repos.UserRepository;
import com.regino.coursework5.server.data.Request;
import com.regino.coursework5.server.data.Response;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Optional;
import java.util.zip.DataFormatException;

@Component
public class Authentication {

    private final UserRepository userRepository = SpringUtils.getBean(UserRepository.class);
    private final CompanyRepository companyRepository = SpringUtils.getBean(CompanyRepository.class);

    public void authorization(Request request, Response response) {
        Optional<User> user = userRepository.findUserByLogin(request.getAttribute("login"));
        if (user.isEmpty()) {
            response.addAttribute("isFind", "false");
        } else {
            if (user.get().getPassword().equals(request.getAttribute("pass"))) {
                response.addAttribute("isAuthorised", "true");
                response.setData(new UserSerDes(user.get()));

                response.addAttribute("clientLogin", user.get().getLogin());
                response.addAttribute("clientStatus", user.get().getAdmin());
            } else {
                response.addAttribute("isAuthorised", "false");
            }
            response.addAttribute("isAdmin", user.get().getAdmin());
        }
    }

    @Transactional
    public void registration(Request request, Response response) {
        char[] bannedSymbols = "!#$%^&*(){}[]'\"\\|;:/?>,<_".toCharArray();
        UserSerDes userReceive = new Gson().fromJson(request.getBody(), UserSerDes.class);
        try {
            for (char ch : bannedSymbols) {
                if (userReceive.getName().indexOf(ch) != -1)
                    throw new DataFormatException("contain banned symbols");
                if (userReceive.getSurname().indexOf(ch) != -1)
                    throw new DataFormatException("contain banned symbols");
                if (userReceive.getEmail().indexOf(ch) != -1)
                    throw new DataFormatException("contain banned symbols");
                if (userReceive.getLogin().indexOf(ch) != -1)
                    throw new DataFormatException("contain banned symbols");
                if (userReceive.getPosition().indexOf(ch) != -1)
                    throw new DataFormatException("contain banned symbols");
                if (userReceive.getStatus().indexOf(ch) != -1)
                    throw new DataFormatException("contain banned symbols");
                if (request.getAttribute("pass").indexOf(ch) != -1)
                    throw new DataFormatException("contain banned symbols");
            }
            if (userReceive.getName().length() < 2)
                throw new DataFormatException("too small name");
            if (userReceive.getSurname().length() < 2)
                throw new DataFormatException("too small surname");

            int dogPos = userReceive.getEmail().indexOf('@');
            int dotPos = userReceive.getEmail().indexOf('.');
            int emailSize = userReceive.getEmail().length();
            if (dogPos == -1 || dotPos == -1 || dotPos == emailSize - 1 || dotPos <= dogPos + 1 || dogPos == 0)
                throw new DataFormatException("wrong email format");

            if (userReceive.getLogin().length() < 5 || Character.isDigit(userReceive.getLogin().charAt(0)))
                throw new DataFormatException("wrong login format");

            if (!verifyPassword(request.getAttribute("pass")))
                throw new DataFormatException("too week password");

            if (userRepository.findUserByLogin(userReceive.getLogin()).isPresent())
                throw new DataFormatException("user with the same login exist");

            Optional<Company> company = companyRepository.findCompanyByCompanyName(userReceive.getCompanyName());
            if (company.isEmpty())
                throw new DataFormatException("company doesn't exist");

            Optional<Location> location = company.get().getLocations()
                    .stream()
                    .filter((n) -> n.getCity().equals(userReceive.getCity()) && n.getCountry().equals(userReceive.getCountry()))
                    .findAny();
            if (location.isEmpty())
                throw new DataFormatException("wrong location");


            User user = new User(
                    userReceive,
                    request.getAttribute("pass"),
                    company.get(),
                    location.get()
            );
            userRepository.save(user);

            response.addAttribute("isSuccess", "true");
        } catch (DataFormatException e) {
            response.addAttribute("exception", e.getMessage());
        }
    }

    public boolean verifyPassword(String pass) {
        if (pass.length() < 6)
            return false;

        int dataTypesContain = 0;
        if (pass.matches(".*[0-9]+.*"))
            ++dataTypesContain;
        if (pass.matches(".*[a-z]+.*"))
            ++dataTypesContain;
        if (pass.matches(".*[A-Z]+.*"))
            ++dataTypesContain;

        if (dataTypesContain < 2)
            return false;


        return true;
    }
}
