package com.regino.coursework5.server.data;

import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class Response {
    private Map<String, String> attrMap = new HashMap<>();
    private String jsonData;

    public void addAttribute(String name, String value) {
        attrMap.put(name, value);
    }
    public void setData(Object jsonData) {
        this.jsonData = new Gson().toJson(jsonData);
    }

    public Map<String, String> getAttrMap() {
        return attrMap;
    }
}
