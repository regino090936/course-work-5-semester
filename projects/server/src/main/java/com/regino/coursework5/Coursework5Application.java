package com.regino.coursework5;

import com.google.gson.Gson;
import com.regino.coursework5.server.ConnectionHandler;
import com.regino.coursework5.server.ConnectionListener;
import com.regino.coursework5.server.RequestDispatcher;
import jdk.swing.interop.SwingInterOpUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootApplication
public class Coursework5Application {

    public static void main(String[] args) {
        SpringApplication.run(Coursework5Application.class, args);
    }

    @Value("${server.port}")
    private int port;

    @Bean
    @Scope("singleton")
    public ServerSocket serverSocket() throws IOException {
        System.out.println("PORT - " + port);
        return new ServerSocket(port);
    }

    @Bean
    @Scope("singleton")
    public ArrayList<ConnectionHandler> getActiveConnections() {
        return new ArrayList<ConnectionHandler>();
    }
}
