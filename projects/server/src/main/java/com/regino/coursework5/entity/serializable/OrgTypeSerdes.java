package com.regino.coursework5.entity.serializable;

public class OrgTypeSerdes {
    private String name;

    public OrgTypeSerdes(String name) {
        this.name = name;
    }

    public OrgTypeSerdes() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
