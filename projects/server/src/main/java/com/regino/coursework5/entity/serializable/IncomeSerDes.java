package com.regino.coursework5.entity.serializable;

import com.regino.coursework5.entity.Income;

public class IncomeSerDes {
    private int year;
    private double winter;
    private double spring;
    private double summer;
    private double autumn;

    public IncomeSerDes(int year, double winter, double spring, double summer, double autumn) {
        this.year = year;
        this.winter = winter;
        this.spring = spring;
        this.summer = summer;
        this.autumn = autumn;
    }

    public IncomeSerDes(Income income) {
        this.year = income.getYear();
        this.winter = income.getWinterIncome();
        this.spring = income.getSpringIncome();
        this.summer = income.getSummerIncome();
        this.autumn = income.getAutumnIncome();
    }

    public IncomeSerDes() {
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getWinter() {
        return winter;
    }

    public void setWinter(double winter) {
        this.winter = winter;
    }

    public double getSpring() {
        return spring;
    }

    public void setSpring(double spring) {
        this.spring = spring;
    }

    public double getSummer() {
        return summer;
    }

    public void setSummer(double summer) {
        this.summer = summer;
    }

    public double getAutumn() {
        return autumn;
    }

    public void setAutumn(double autumn) {
        this.autumn = autumn;
    }
}
