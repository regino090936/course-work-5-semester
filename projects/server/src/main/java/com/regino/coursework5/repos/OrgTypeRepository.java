package com.regino.coursework5.repos;

import com.regino.coursework5.entity.Company;
import com.regino.coursework5.entity.OrgType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface OrgTypeRepository extends CrudRepository<OrgType, Integer> {
    Optional<OrgType> findOrgTypeByType(String type);
    List<OrgType> findAllByTypeStartsWith(String template);
    Optional<OrgType> findOrgTypeByCompaniesContains(Company company);
}
